
import os
import time
from unittest import result
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
prefs = {"download.default_directory": "/usr/"}
options = webdriver.ChromeOptions()
options.add_experimental_option("prefs", prefs)

driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")

ico_list = ["27082440", "27082440", "27082440"]
year = "2021"
search_word = "účetní závěrka"


driver.get("https://or.justice.cz/ias/ui/rejstrik-$firma")

def latest_download_file():
            path = r'/usr/'
            os.chdir(path)
            files = sorted(os.listdir(os.getcwd()), key=os.path.getmtime)
            newest = files[-1]
            return newest

def download_doc():
        for q in results:
            print(q)
        # to identify table rows
        s_r = driver.find_elements(By.XPATH, "//table/tbody/tr")
        # to identify table columns
        last_row = len(s_r)
        s_c = driver.find_element(
            By.XPATH, "//table/tbody/tr["+str(last_row)+"]/td/a")
        print(len(s_r))
        print(s_c.text)
        s_c.click()

        

        fileends = "crdownload"
        while "crdownload" == fileends:
            time.sleep(1)
            newest_file = latest_download_file()
            if "crdownload" in newest_file:
                fileends = "crdownload"
            else:
                fileends = "none"
        back_to_docs = driver.find_element(By.CLASS_NAME, "back")
        back_to_docs.click()

def back():
        print("Další nebo žádný soubor nebyl nalezen!")  
        print("Jdu na hledat další společnost!")      
        back_to_search = driver.find_element(By.XPATH, "//*[@class= 'inner']/*[@class= 'noprint']/a")
        back_to_search.click()
        time.sleep(5)

for ico in ico_list:
    driver.find_element(By.ID, 'id3').clear() 
    search = driver.find_element(By.ID, 'id3')
    search.send_keys(ico)
    search.send_keys(Keys.RETURN)
    search = driver.find_element(By.LINK_TEXT, 'Sbírka listin')
    search.click()

    # to identify table rows
    r = driver.find_elements(By.XPATH, "//*[@class= 'list']/tbody/tr")
    
    # to get row count with len method
    rc = len(r)

    results = []
    # to traverse through the table rows excluding headers
    for i in range(1, rc + 1):

        # to get all the cell data with text method

        second_column = driver.find_element(
            By.XPATH, "//tr["+str(i)+"]/td[2]").text
        third_column = driver.find_element(
            By.XPATH, "//tr["+str(i)+"]/td[3]").text
        # to search for our required text
        if((second_column.find(search_word) != -1) & (third_column.find(year) != -1)):
            results.append(driver.find_element(
                By.XPATH, "//tr["+str(i)+"]/td[1]/a").text)
            first_column = driver.find_element(
                By.XPATH, "//tr["+str(i)+"]/td[1]/a")
            first_column.click()
            download_doc()
    back()
    # check if list size greater than 0
    
    

print("Všechny požadavky zpracovány")
print("Zavírám program!")
driver.quit()
# =========================================================================================================
